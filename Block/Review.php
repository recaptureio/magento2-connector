<?php

namespace Recapture\Connector\Block;

use Magento\Framework\View\Element\Template;

class Review extends Template
{
  protected $_ratings = null;
  protected $_order = null;
  protected $_orderItems = null;
  protected $_orderInterface = null;
  protected $_reviewModel = null;
  protected $_logo = null;
  protected $_context = null;
  protected $_ratingFactory = null;
  protected $configurableProduct = null;
  protected $productLinks = null;
  protected $secureRenderer;

  public function __construct(
    \Magento\Framework\View\Element\Template\Context $context,
    \Magento\Sales\Api\Data\OrderInterface $order,
    \Magento\Review\Model\Review $review,
    \Magento\Catalog\Model\ProductFactory $productloader,
    \Magento\Theme\Block\Html\Header\Logo $logo,
    \Magento\Review\Model\RatingFactory $ratingFactory,
    \Magento\Catalog\Model\ProductRepository $productRepository,
    \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurableProduct,
    \Recapture\Connector\Helper\Transport $transport,
    \Magento\GroupedProduct\Model\ResourceModel\Product\Link $productLinks,
    \Magento\Framework\View\Helper\SecureHtmlRenderer $secureRenderer,
    array $data = []
  ) {
    $this->_context = $context;
    $this->_orderInterface = $order;
    $this->_reviewModel = $review;
    $this->_productloader = $productloader;
    $this->_logo = $logo;
    $this->_ratingFactory = $ratingFactory;
    $this->_productRepository = $productRepository;
    $this->_pageConfig = $context->getPageConfig();
    $this->transport = $transport;
    $this->configurableProduct = $configurableProduct;
    $this->productLinks = $productLinks;
    $this->secureRenderer = $secureRenderer;

    parent::__construct($context, $data);
  }

  public function _beforeToHtml(){
    if (empty($this->_order)){
      $this->_order = $this->_orderInterface->loadByIncrementId($this->getOrderId());
    }

    if (empty($this->_orderItems)) {
      $this->_orderItems = $this->_order->getAllVisibleItems();
    }
  }

  protected function _prepareLayout()
  {
    $this->_pageConfig->getTitle()->set('Leave a Review');
    return parent::_prepareLayout();
  }

  public function getOrder(){
    return $this->_order;
  }

  public function getLogoUrl() {
    return $this->_logo->getLogoSrc();
  }

  public function getBaseUrl() {
    return $this->_storeManager->getStore()->getBaseUrl();
  }

  public function getProduct($productId) {
    $product = $this->_productloader->create()->load($productId);

    $parentIds = $this->productLinks->getParentIdsByChild(
      $productId,
      \Magento\GroupedProduct\Model\ResourceModel\Product\Link::LINK_TYPE_GROUPED
    );

    // if we have a parent product then load
    if (sizeof($parentIds) > 0) {
      return $this->_productloader->create()->load($parentIds[0]);
    }

    return $product;
  }

  public function escapeHtml($html, $allowedTags = NULL) {
    return $this->_context->getEscaper()->escapeHtml($html, $allowedTags);
  }

  public function getProductUrl($id) {
    $product = $this->_productRepository->getById($id);
    return $product->getUrlModel()->getUrl($product);
  }

  public function getRatings() {
    if (empty($this->_ratings)) {
      $ratingCollection = $this->_ratingFactory
        ->create()
        ->getResourceCollection()
        ->addEntityFilter('product')
        ->setPositionOrder()
        ->addRatingPerStoreName($this->_storeManager->getStore()->getId())
        ->setStoreFilter($this->_storeManager->getStore()->getId())
        ->load()
        ->addOptionToItems();
        $this->_ratings = $ratingCollection;
      }

      return $this->_ratings;
  }

  public function getOrderItems(){
    return $this->_orderItems;
  }

  public function getPageText() {
    $result = $this->transport->dispatchWithLongTimeout('reviews/page-text');
    $pageText = json_decode($result->getBody());
    return $pageText;
  }

  public function getProductImage($product) {
    return $this->getUrl('pub/media/catalog').'product'.$product->getImage();
  }

  public function registerScript($scriptContent) {
    return $this->secureRenderer->renderTag('script', [], $scriptContent, false);
  }
}
