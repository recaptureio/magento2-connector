<?php

namespace Recapture\Connector\Block;

use Magento\Framework\View\Element\Template;

class ReviewSuccess extends Template
{
  protected $_ratings = null;
  protected $_order = null;
  protected $_orderItems = null;
  protected $_orderInterface = null;
  protected $_reviewModel = null;
  protected $_logo = null;
  protected $_context = null;
  protected $_ratingFactory = null;
  protected $secureRenderer = null;

  public function __construct(
    \Magento\Framework\View\Element\Template\Context $context,
    \Magento\Sales\Api\Data\OrderInterface $order,
    \Magento\Review\Model\Review $review,
    \Magento\Catalog\Model\ProductFactory $productloader,
    \Magento\Theme\Block\Html\Header\Logo $logo,
    \Magento\Review\Model\RatingFactory $ratingFactory,
    \Recapture\Connector\Helper\Transport $transport,
    \Magento\Framework\View\Helper\SecureHtmlRenderer $secureRenderer,
    array $data = []
  ) {
    $this->_context = $context;
    $this->_orderInterface = $order;
    $this->_reviewModel = $review;
    $this->_productloader = $productloader;
    $this->_logo = $logo;
    $this->_ratingFactory = $ratingFactory;
    $this->_pageConfig = $context->getPageConfig();
    $this->transport = $transport;
    $this->secureRenderer = $secureRenderer;

    parent::__construct($context, $data);
  }

  public function getLogoUrl() {
    return $this->_logo->getLogoSrc();
  }

  protected function _prepareLayout()
  {
    $this->_pageConfig->getTitle()->set('Thank you!');
    return parent::_prepareLayout();
  }

  public function getBaseUrl() {
    return $this->_storeManager->getStore()->getBaseUrl();
  }

  public function getPageText() {
    $result = $this->transport->dispatchWithLongTimeout('reviews/page-text');
    $pageText = json_decode($result->getBody());
    return $pageText;
  }

  public function registerScript($scriptContent) {
    return $this->secureRenderer->renderTag('script', [], $scriptContent, false);
  }
}
