<?php

namespace Recapture\Connector\Block;

class Invalidator extends \Magento\Customer\Block\CustomerData {

    protected $helper;
    protected $secureRenderer;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Recapture\Connector\Helper\Invalidator $helper,
        \Magento\Framework\View\Helper\SecureHtmlRenderer $secureRenderer
    ) {
        $this->helper      = $helper;
        $this->secureRenderer = $secureRenderer;

        parent::__construct($context, []);
    }

    public function getInvalidations(){
        return $this->helper->getInvalidations();
    }

    public function registerScript($scriptContent) {
        return $this->secureRenderer->renderTag('script', [], $scriptContent, false);
    }
}
