<?php

namespace Recapture\Connector\Block;

class Client extends \Magento\Framework\View\Element\Template {

    protected $helper;
    protected $registry;
    protected $secureRenderer;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Recapture\Connector\Helper\Data $helper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\View\Helper\SecureHtmlRenderer $secureRenderer,
    ) {
        $this->helper = $helper;
        $this->registry = $registry;
        $this->secureRenderer = $secureRenderer;
        parent::__construct($context, []);
    }

    public function isBotOrSpeedTest() {
        return (
            isset($_SERVER['HTTP_USER_AGENT'])
            && preg_match('/googlebot|chrome-lighthouse/i', $_SERVER['HTTP_USER_AGENT'])
        );
    }

    public function registerScript($scriptContent) {
        return $this->secureRenderer->renderTag('script', [], $scriptContent, false);
    }

    public function shouldTrack(){

        // If this is a bot or a speed test, don't include the script
        if ($this->isBotOrSpeedTest()) {
            return false;
        }

        if (!$this->helper->isEnabled()) {
            return false;
        }

        $apiKey = $this->getApiKey();
        if (empty($apiKey)) {
            return false;
        }

        // check excluded pages
        $excludedPages = $this->helper->getStoreConfig('recapture/advanced/exclude_js_pages');

        if ($excludedPages != null) {
            $pages = preg_split("/\r\n|\n|\r/", $excludedPages);
            $currentPage = $_SERVER['REQUEST_URI'];

            foreach ($pages as $page) {
                if (fnmatch($page, $currentPage)) {
                    return false;
                }
            }
        }

        return true;

    }

    public function shouldTrackEmail(){

        if (!$this->shouldTrack()) return false;
        if (!$this->helper->canTrackEmail()) return false;

        return true;

    }

    public function getApiKey(){

        return $this->helper->getApiKey();

    }

    public function getCurrentProduct(){

        return $this->registry->registry('current_product');

    }

    public function getAnalyticsUrl(){

        $queueUrl = $this->helper->getAnalyticsUrl();

        //append a timestamp that changes every 10 minutes
        $queueUrl .= '?v=' . round(time() / (60 * 10));

        return $queueUrl;

    }

}
