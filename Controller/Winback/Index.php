<?php

namespace Recapture\Connector\Controller\Winback;

use Recapture\Connector\Model\Landing;

class Index extends \Magento\Framework\App\Action\Action {

    protected $helper;
    protected $urlInterface;
    protected $formKey;   
    protected $cart;
    protected $product;
    protected $checkoutSession;
    protected $order;
    protected $invalidator;
    protected $transport;
    protected $productRepo;
    protected $storeManager;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Recapture\Connector\Helper\Data $helper,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Model\Product $product,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Api\Data\OrderInterface $order,
        \Recapture\Connector\Helper\Invalidator $invalidator,
        \Recapture\Connector\Helper\Transport $transport,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepo,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->urlInterface = $context->getUrl();
        $this->helper = $helper;
        $this->formKey = $formKey;
        $this->cart = $cart;
        $this->product = $product;
        $this->checkoutSession = $checkoutSession;
        $this->order = $order;
        $this->invalidator = $invalidator;
        $this->transport = $transport;
        $this->productRepo = $productRepo;
        $this->storeManager = $storeManager;

        parent::__construct($context);
    }

    public function redirectWithUtm($url) {
        return $this->_redirect(
            $url, array('_query'=>$_SERVER['QUERY_STRING'])
        );
    }

    private function _translateOrderHash($hash = null) {
        if (empty($hash)) {
          return false;
        }
    
        $result = $this->transport->dispatchWithLongTimeout('order/retrieve', array(
          'hash' => $hash
        ));
    
        $body = json_decode($result->getBody());
    
        if ($body->status == 'success') {
          return $body->data->order_id;
        }
    
        return false;
      }

    public function execute() {
        // http://172.16.83.128/recapture/winback?hash=fb634d1c7dce78d544fbb7b7eada0d9825270516f6d0f78e8435eff51fbd072277fa7e19d0bfda04772106a42ca8010dfaf6a0cd51fdd9e6443ed18a4ca62476

        $this->cart->truncate();

        try {
            $hash = $this->getRequest()->getParam('hash');
            $orderId = $this->_translateOrderHash($hash);
        } catch (\Exception $e){
        }

        $storeId = $this->storeManager->getStore()->getId();
        $order = $this->order->loadByIncrementId($orderId);

        if (!$order->getId()) {
            return $this->redirectWithUtm('/');
        }

        $items = $order->getItemsCollection();
 
        foreach ($items as $item) {
            // get the product
            try {
                $product = $this->productRepo->getById($item->getProductId(), false, $storeId, true);

                if ($product->isSalable()) {
                    $this->cart->addOrderItem($item);
                }
            } catch (\Exception $e){
                // problem with this product
            }
        }
 
        $this->cart->save();
        $this->invalidator->invalidate('customer')->invalidate('cart');

        switch ($this->helper->getReturnLanding()) {
            case Landing::REDIRECT_HOME:
                return $this->redirectWithUtm('/');

            case Landing::REDIRECT_CHECKOUT:
                return $this->redirectWithUtm(
                    $this->urlInterface->getUrl('checkout', ['_secure' => true, '_query'=>$_SERVER['QUERY_STRING']])
                );

            case Landing::REDIRECT_CART:
            default:
                return $this->redirectWithUtm(
                    $this->urlInterface->getUrl('checkout/cart', ['_secure' => true, '_query'=>$_SERVER['QUERY_STRING']])
                );
        }
    }
}
