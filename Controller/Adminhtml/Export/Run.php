<?php

namespace Recapture\Connector\Controller\Adminhtml\Export;

use \Magento\Backend\App\Action\Context;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\Json\Helper\Data;
use \Psr\Log\LoggerInterface;
use \Magento\Framework\App\Request\Http;
use \Magento\Reports\Model\ResourceModel\Quote\Collection;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Sales\Model\OrderFactory;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;

class Run extends \Magento\Backend\App\Action {
    protected $resultPageFactory;
    protected $jsonHelper;
    protected $request;
    protected $logger;
    protected $reportColl;
    protected $storeManager;
    protected $orderFactory;
    protected $orderCollectionFactory;
    protected $collection;
    protected $quoteHelper;
    protected $transport;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Data $jsonHelper,
        LoggerInterface $logger,
        Http $request,
        Collection $collection,
        StoreManagerInterface $storeManager,
        OrderFactory $orderFactory,
        CollectionFactory $orderCollectionFactory,
        \Recapture\Connector\Helper\Quote $quoteHelper,
        \Recapture\Connector\Helper\Transport $transport
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        $this->request = $request;
        $this->collection = $collection;
        $this->storeManager = $storeManager;
        $this->quoteHelper = $quoteHelper;
        $this->transport = $transport;
        $this->orderFactory = $orderFactory;
        $this->orderCollectionFactory = $orderCollectionFactory;

        parent::__construct($context);
    }

    public function exportCarts() {
        try {
            $store = $this->request->getParam('store');

            // get the abandoned carts
            $this->collection->prepareForAbandonedReport(
                strlen($store)
                    ? [$store]
                    : []
            );

            // get the report data
            $rows = $this->collection->load();

            // convert to carts
            $carts = [];
            foreach ($rows as $quote) {
                $cart = $this->quoteHelper->getCartFromQuote(
                    $quote,
                    $this->storeManager->getStore($store)
                );
                // Created and updated fields
                $cart['created_at'] = $quote->getCreatedAt();
                $cart['updated_at'] = $quote->getUpdatedAt();

                // add to the array
                $carts[] = $cart;
            }

            // send to recapture
            $res = $this->transport->dispatch(
                'import-magento-carts',
                ['carts' => $carts],
                120
            );

            return $this->jsonResponse([
                'ok' => true,
                'res' => $res
            ]);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    public function getOrderCollection() {
        $to = date('Y-m-d h:i:s');
        $from = date(
            'Y-m-d h:i:s',
            strtotime('-30 day', strtotime($to))
        );

        return $this->orderCollectionFactory->create()
            ->addFieldToFilter(
                'created_at', [
                    'from' => $from,
                    'to' => $to
                ]);
    }

    public function getOrderCount() {
        $count = $this->getOrderCollection()
            ->getSize();

        return $this->jsonResponse($count);
    }

    public function exportOrderPage() {
        try {
            $pageSize = $this->request->getParam('pageSize');
            $page = $this->request->getParam('page');
            $store = $this->request->getParam('store');

            if (strlen($pageSize) == 0 || strlen($page) == 0) {
                return $this->jsonResponse([
                    'ok' => false,
                    'error' => 'Please provide the page size and index'
                ]);
            }

            $rows = $this->getOrderCollection()
                ->setPageSize(intval($pageSize))
                ->setCurPage(intval($page))
                ->addFieldToSelect('*')
                ->setOrder(
                    'created_at',
                    'desc'
                );

            $orders = [];
            foreach ($rows as $row) {
                $cart = $this->quoteHelper->getCartFromOrder(
                    $row,
                    $this->storeManager->getStore($store)
                );

                // Created and updated fields
                $cart['created_at'] = $row->getCreatedAt();
                $cart['updated_at'] = $row->getUpdatedAt();

                // add to the array
                $orders[] = [
                    'cart' => $cart,
                    'external_id' => $row->getId(),
                    'created_at' => $row->getCreatedAt(),
                    'updated_at' => $row->getUpdatedAt()
                ];
            }

            // send to recapture
            $res = $this->transport->dispatch(
                'import-magento-orders',
                ['orders' => $orders],
                120
            );

            return $this->jsonResponse([
                'ok' => true
            ]);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    public function execute() {
        $action = $this->request->getParam('action');

        switch ($action) {
            case 'abandoned_carts':
                return $this->exportCarts();

            case 'get_order_count':
                return $this->getOrderCount();

            case 'export_orders_page':
                return $this->exportOrderPage();

            default:
                return $this->jsonResponse([
                    'ok' => false,
                    'error' => 'Unknown action'
                ]);
        }
    }

    public function jsonResponse($response = '') {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}