<?php
namespace Recapture\Connector\Controller\Adminhtml\Export;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use \Magento\Store\Model\StoreManagerInterface;
use \Recapture\Connector\Helper\Data;

class Index extends Action implements HttpGetActionInterface
{
    protected $resultPageFactory;
    protected $storeManager;
    protected $helper;
    protected $secureRenderer;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        StoreManagerInterface $storeManager,
        Data $helper,
        \Magento\Framework\View\Helper\SecureHtmlRenderer $secureRenderer,
    ) {
        parent::__construct($context);

        $this->resultPageFactory = $resultPageFactory;
        $this->storeManager = $storeManager;
        $this->helper = $helper;
        $this->secureRenderer = $secureRenderer;
    }

    public function execute() {
        // Disable exporting from the default scope
        if (
            $this->helper->getCurrentScope() == 'default' &&
            $this->helper->canAuthenticateDefaultView() != true
        ) {
            $this->messageManager->addError('You cannot export data from the Default Config scope. Please change the Store View on the top left to a specific website before exporting.');

            $redirect = $this->resultRedirectFactory->create();
            $redirect->setPath('adminhtml/system_config/edit', array('section' => 'recapture'));
            return $redirect;
        }

        $page = $this->resultPageFactory->create();
        $page->getConfig()->getTitle()->prepend(__('Recapture Export'));

        $block = $page->getLayout()->getBlock('recapture.export.index');
        $block->setData('scope', $this->helper->getScopeForUrl());
        $block->setData('secureRenderer', $this->secureRenderer);

        return $page;
    }

    public function registerScript($scriptContent){
        return $this->secureRenderer->renderTag('script', [], $scriptContent, false);
    }

    public function getScope() {
        return json_encode($this->helper->getScopeForUrl());
    }
}