<?php

namespace Recapture\Connector\Controller\Api;

use \Magento\Framework\Controller\Result\JsonFactory;
use \Magento\Framework\App\Action\Context;
use \Magento\Catalog\Helper\Image;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\Pricing\Helper\Data;
use \Magento\Catalog\Model\ProductFactory;
use \Magento\ConfigurableProduct\Model\Product\Type\Configurable;

use \Recapture\Connector\Helper\Api;

class ProductById extends \Magento\Framework\App\Action\Action {

    protected $api;
    protected $resultJsonFactory;
    protected $priceHelper;
    protected $productFactory;
    protected $configurableProduct;

    public function __construct(
        Context $context,
        Api $api,
        JsonFactory $resultJsonFactory,
        StoreManagerInterface $storeManager,
        Data $priceHelper,
        ProductFactory $productFactory,
        Configurable $configurableProduct
    ) {
        $this->api = $api;
        $this->resultJsonFactory = $resultJsonFactory;

        $this->storeManager = $storeManager;
        $this->priceHelper = $priceHelper;
        $this->productFactory = $productFactory;
        $this->configurableProduct = $configurableProduct;

        parent::__construct($context);
    }

    public function execute(){
        $result = $this->resultJsonFactory->create();

        // Verify the API key
        if (!$this->api->validateAuthToken($this->getRequest()->getParam('authToken'))) {
            $result->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_FORBIDDEN);
            $result->setData(['ok' => false]);
            return $result;
        }

        $productId = $this->getRequest()->getParam('id', null);

        if ($productId == null) {
            $result->setData([
                'ok' => false,
                'product' => null
            ]);
            return $result;
        }

        $store = $this->storeManager->getStore();
        $imageBaseUrl = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'catalog/product';

        $priceHelper = $this->priceHelper;
        $product = $this->productFactory->create()->load($productId);

        if (!$product || $product->getId() == null) {
            $result->setData([
                'ok' => false,
                'product' => null
            ]);
            return $result;
        }

        $productUrl = $product->getUrlModel()->getUrl($product);

        // If we have a parent and the current product is not visible try to use the parent
        $parentProductIds = $this->configurableProduct->getParentIdsByChild($product->getId());

        if (!$product->isVisibleInSiteVisibility() && isset($parentProductIds[0])) {
            $parentProduct = $this->productFactory->create()->load($parentProductIds[0]);
            if ($parentProduct && $parentProduct->getId() != null) {
                $productUrl = $parentProduct->getUrlModel()->getUrl($parentProduct);
            }
        }

        $result->setData([
            'ok' => true,
            'product' => [
                'id' => $product->getId(),
                'name' => $product->getName(),
                'sku' => $product->getSku(),
                'url' => $productUrl,
                'image' => $imageBaseUrl.$product->getImage(),
                'price' => $priceHelper->currency($product->getPrice(), false, false),
                'displayPrice' => $priceHelper->currency($product->getPrice(), true, false),
            ]
        ]);

        return $result;
    }
}
