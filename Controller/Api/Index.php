<?php

namespace Recapture\Connector\Controller\Api;

class Index extends \Magento\Framework\App\Action\Action {
    protected $api;
    protected $resultJsonFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Recapture\Connector\Helper\Api $api,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory

    ) {
        $this->api = $api;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute() {
        $result = $this->resultJsonFactory->create();

        // Verify the API key
        if (!$this->api->validateAuthToken($this->getRequest()->getParam('authToken'))) {
            $result->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_FORBIDDEN);
            $result->setData(['ok' => false]);
            return $result;
        }

        $result->setData(['ok' => true]);
        return $result;
    }
}
