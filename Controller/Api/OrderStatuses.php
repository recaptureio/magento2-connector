<?php

namespace Recapture\Connector\Controller\Api;

use \Magento\Framework\Controller\Result\JsonFactory;
use \Magento\Framework\App\Action\Context;
use \Magento\Sales\Model\ResourceModel\Order\Status\Collection as OrderStatusCollection;

use \Recapture\Connector\Helper\Api;

class OrderStatuses extends \Magento\Framework\App\Action\Action {

    protected $api;
    protected $resultJsonFactory;
    protected $orderStatusCollection;

    public function __construct(
        Context $context,
        Api $api,
        JsonFactory $resultJsonFactory,
        OrderStatusCollection $orderStatusCollection
    ) {
        $this->api = $api;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->orderStatusCollection = $orderStatusCollection;

        parent::__construct($context);
    }

    public function execute(){
        $result = $this->resultJsonFactory->create();

        // Verify the API key
        if (!$this->api->validateAuthToken($this->getRequest()->getParam('authToken'))) {
            $result->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_FORBIDDEN);
            $result->setData(['ok' => false]);
            return $result;
        }

        $result->setData([
            'ok' => true,
            'statuses' => $this->orderStatusCollection->toOptionArray()
        ]);

        return $result;
    }
}
