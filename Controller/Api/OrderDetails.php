<?php

namespace Recapture\Connector\Controller\Api;

use \Magento\Framework\Controller\Result\JsonFactory;
use \Magento\Framework\App\Action\Context;

use \Recapture\Connector\Helper\Api;

class OrderDetails extends \Magento\Framework\App\Action\Action {

    protected $api;
    protected $resultJsonFactory;
    protected $orderStatusCollection;
    protected $orderInterface;
    protected $helper;    

    public function __construct(
        Context $context,
        Api $api,
        JsonFactory $resultJsonFactory,
        \Magento\Sales\Api\Data\OrderInterface $orderInterface,
        \Psr\Log\LoggerInterface $logger,
        \Recapture\Connector\Helper\Data $helper
    ) {
        $this->api = $api;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->orderInterface = $orderInterface;
        $this->logger = $logger;
        $this->helper = $helper;

        parent::__construct($context);
    }

    public function execute(){
        $result = $this->resultJsonFactory->create();

        // Verify the API key
        if (!$this->api->validateAuthToken($this->getRequest()->getParam('authToken'))) {
            $result->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_FORBIDDEN);
            $result->setData(['ok' => false]);
            return $result;
        }

        $orderId = $this->getRequest()->getParam('id', null);

        if ($orderId == null) {
            $result->setData([
                'ok' => false,
                'product' => null
            ]);
            return $result;
        }

        $this->logger->info("Recapture: Loading order ID (OrderID:$orderId)");

        $order = $this->orderInterface->loadByIncrementId($orderId);

        if (!$order) {
            $result->setData([
                'ok' => false,
                'product' => null
            ]);
            return $result;
        }

        $shipping_address = $order->getShippingAddress();
        $billing_address = $order->getBillingAddress();

        // get the payment method
        $payment = $order->getPayment();
        $paymentMethod = $payment->getMethodInstance();

        $data = [
            'external_id'  => $order->getQuoteId(),
            'order_id' => $order->getIncrementId(),
            'state' => $order->getState(),
            'status'=> $order->getStatus(),
            'shipping_address' => $shipping_address
                ? $this->helper->getAddress($shipping_address)
                : null,
            'billing_address' => $billing_address
                ? $this->helper->getAddress($billing_address)
                : null,
            'payment_method' => [
                'code' => $paymentMethod->getCode(),
                'name' => $paymentMethod->getTitle()
            ],
            'shipping_method' => [
                'code' => $order->getShippingMethod(),
                'name' => $order->getShippingDescription()
            ],
            'totals' => $this->helper->getOrderTotals($order)
        ];

        $result->setData([
            'ok' => true,
            'order' => $data
        ]);

        return $result;
    }
}
