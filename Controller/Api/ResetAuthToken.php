<?php

namespace Recapture\Connector\Controller\Api;

use \Magento\Framework\Controller\Result\JsonFactory;
use \Magento\Framework\App\Action\Context;
use \Magento\Catalog\Helper\Image;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\Pricing\Helper\Data;
use \Magento\Catalog\Model\ProductFactory;

use \Recapture\Connector\Helper\Api;

class ResetAuthToken extends \Magento\Framework\App\Action\Action {

    protected $api;
    protected $resultJsonFactory;

    public function __construct(
        Context $context,
        Api $api,
        JsonFactory $resultJsonFactory
    ) {
        $this->api = $api;
        $this->resultJsonFactory = $resultJsonFactory;

        parent::__construct($context);
    }

    public function execute(){
        $result = $this->resultJsonFactory->create();

        // Verify the API key
        if (!$this->api->validateAuthToken($this->getRequest()->getParam('authToken'))) {
            $result->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_FORBIDDEN);
            $result->setData(['ok' => false]);
            return $result;
        }

        // Reset the auth token
        $this->api->resetAuthToken();

        $result->setData(['ok' => true]);
        return $result;
    }
}
