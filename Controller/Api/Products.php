<?php

namespace Recapture\Connector\Controller\Api;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use \Magento\Framework\Controller\Result\JsonFactory;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\Api\SortOrderBuilder;
use \Magento\Catalog\Helper\Image;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\Pricing\Helper\Data;
use \Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use \Magento\Catalog\Model\ProductFactory;

use \Recapture\Connector\Helper\Api;

class Products extends \Magento\Framework\App\Action\Action {

    protected $api;
    protected $resultJsonFactory;
    protected $searchCriteriaBuilder;
    protected $filterBuilder;
    protected $productRepository;
    protected $filterGroupBuilder;
    protected $sortOrderBuilder;
    protected $storeManager;
    protected $priceHelper;
    protected $configurableProduct;
    protected $productFactory;
    protected $imageHelper;
    protected $productResource;
    protected $mediaConfig;

    public function __construct(
        Context $context,
        Api $api,
        JsonFactory $resultJsonFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        ProductRepository $productRepository,
        FilterGroupBuilder $filterGroupBuilder,
        SortOrderBuilder $sortOrderBuilder,
        StoreManagerInterface $storeManager,
        Configurable $configurableProduct,
        ProductFactory $productFactory,
        Data $priceHelper,
        Image $imageHelper,
        \Magento\Catalog\Model\ResourceModel\Product $productResource,
        \Magento\Catalog\Model\Product\Media\Config $mediaConfig
    ) {
        $this->api = $api;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->productRepository = $productRepository;
        $this->filterGroupBuilder = $filterGroupBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->storeManager = $storeManager;
        $this->priceHelper = $priceHelper;
        $this->configurableProduct = $configurableProduct;
        $this->productFactory = $productFactory;
        $this->imageHelper = $imageHelper;
        $this->productResource = $productResource;
        $this->mediaConfig = $mediaConfig;

        parent::__construct($context);
    }

    public function execute(){
        $result = $this->resultJsonFactory->create();

        // Verify the API key
        if (!$this->api->validateAuthToken($this->getRequest()->getParam('authToken'))) {
            $result->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_FORBIDDEN);
            $result->setData(['ok' => false]);
            return $result;
        }

        $search = $this->getRequest()->getParam('query', '');
        $page = $this->getRequest()->getParam('page', 1);
        $limit = $this->getRequest()->getParam('limit', 30);

        $filterSku = $this
            ->filterBuilder
            ->setField('sku')
            ->setConditionType('like')
            ->setValue('%'.$search.'%')
            ->create();

        $filterName = $this
            ->filterBuilder
            ->setField('name')
            ->setConditionType('like')
            ->setValue('%'.$search.'%')
            ->create();

        $filterGroup1 = $this
            ->filterGroupBuilder
            ->addFilter($filterSku)
            ->addFilter($filterName)->create();

        $searchCriteria = $this
            ->searchCriteriaBuilder
            ->setFilterGroups([$filterGroup1])->create();

        $sortOrder = $this->sortOrderBuilder
            ->setField('name')
            ->setDirection('ASC')
            ->create();

        $searchCriteria->setSortOrders([$sortOrder]);
        $searchCriteria->setCurrentPage($page);
        $searchCriteria->setPageSize($limit);

        $searchResults = $this
            ->productRepository
            ->getList($searchCriteria);

        $products = $searchResults->getItems();

        $store = $this->storeManager->getStore();

        $priceHelper = $this->priceHelper;
        $configurableProduct = $this->configurableProduct;
        $productFactory = $this->productFactory;
        $imageHelper = $this->imageHelper;
        $productResource = $this->productResource;
        $mediaConfig = $this->mediaConfig;

        $result->setData([
            'ok' => true,
            'products' => array_values(array_map(function($product) use (
                $priceHelper,
                $configurableProduct,
                $productFactory,
                $imageHelper,
                $productResource,
                $store,
                $mediaConfig
            ) {
                $productUrl = $product->getUrlModel()->getUrl($product);

                // If we have a parent and the current product is not visible try to use the parent
                $parentProductIds = $configurableProduct->getParentIdsByChild($product->getId());

                if (!$product->isVisibleInSiteVisibility() && isset($parentProductIds[0])) {
                    $parentProduct = $productFactory->create()->load($parentProductIds[0]);
                    if ($parentProduct && $parentProduct->getId() != null) {
                        $productUrl = $parentProduct->getUrlModel()->getUrl($parentProduct);
                    }
                }

                $image = $productResource->getAttributeRawValue($product->getId(), 'thumbnail', $store);

                $imageUrl = ($image && $image != 'no_selection')
                    ? $mediaConfig->getMediaUrl($image)
                    : '';

                return [
                    'id' => $product->getId(),
                    'name' => $product->getName(),
                    'sku' => $product->getSku(),
                    'url' => $productUrl,
                    'image' => $imageUrl,
                    'price' => $priceHelper->currency($product->getPrice(), false, false),
                    'displayPrice' => $priceHelper->currency($product->getPrice(), true, false)
                ];
            }, $products))
        ]);

        return $result;
    }
}
