<?php

namespace Recapture\Connector\Controller\Review;

use Recapture\Connector\Model\Landing;

class Submit extends \Magento\Framework\App\Action\Action
{
  protected $helper;
  protected $transport;
  protected $logger;
  protected $messageManager;
  protected $urlInterface;
  protected $invalidator;
  protected $_orderInterface;
  protected $_review;
  protected $_rating;

  public function __construct(
    \Magento\Framework\App\Action\Context $context,
    \Recapture\Connector\Helper\Data $helper,
    \Recapture\Connector\Helper\Invalidator $invalidator,
    \Recapture\Connector\Helper\Transport $transport,
    \Magento\Sales\Api\Data\OrderInterface $order,
    \Magento\Review\Model\Review $review,
    \Magento\Review\Model\Rating $rating,
    \Psr\Log\LoggerInterface $logger
  )
  {
    $this->helper       = $helper;
    $this->invalidator  = $invalidator;
    $this->transport    = $transport;
    $this->logger       = $logger;
    $this->urlInterface = $context->getUrl();
    $this->_orderInterface = $order;
    $this->_review = $review;
    $this->_rating = $rating;

    parent::__construct($context);
  }

  public function execute()
  {
    if (!$this->helper->isEnabled() || !$this->helper->getApiKey()){
      return $this->_redirect('/');
    }

    $orderId = false;

    try {
      $hash = $this->getRequest()->getParam('hash');
      $orderId = $this->_translateOrderHash($hash);
    }
    catch (\Exception $e) {
    }

    if (!$orderId) {
      $this->messageManager->addError('There was an error retrieving your order.');
      return $this->_redirect('/');
    }

    $order = $this->_orderInterface->loadByIncrementId($orderId);
    $items = $this->getRequest()->getParam('items');
    $submittedReviews = array();

    foreach ($items as $productId => $item) {

      if ($item['skip'] == 1)
        continue;

      $data             = array();
      $data['title']    = $item['title'];
      $data['detail']   = $item['detail'];
      $data['sku']      = $item['sku'];
      $data['name']     = $item['name'];
      $data['nickname'] = empty($item['nickname']) ? 'N/A' : $item['nickname'];
      $data['ratings']  = array();
      $data['product_url'] = $item['product_url'];
      $ratings          = $item['ratings'];

      $review = $this->_review->setData($data);
      $entityId = $review->getEntityIdByCode(
        \Magento\Review\Model\Review::ENTITY_PRODUCT_CODE
      );

      $review
        ->setEntityId($entityId)
        ->setCustomerId($order->getCustomerId())
        ->setEntityPkValue($productId)
        ->setStatusId(\Magento\Review\Model\Review::STATUS_PENDING)
        ->setStoreId($item['store_id'])
        ->setStores(array($item['store_id']))
        ->save();

      foreach ($ratings as $ratingId => $optionId) {
        $rating = $this->_rating->load($ratingId);
        $options = $rating->getOptions();

        foreach ($options as $option) {
          if ($option->getOptionId() == $optionId) {
            $data['ratings'][] = array(
              'label' => $rating->getRatingCode(),
              'value' => $option->getValue()
            );
            break;
          }
        }

        $this->_rating
          ->setRatingId($ratingId)
          ->setReviewId($review->getId())
          ->addOptionVote($optionId, $productId);
      }

      $review->aggregate();
      $submittedReviews[] = $data;
    }

    $transportData = array(
      'external_id' => $orderId,
      'reviews' => $submittedReviews
    );

    $result = $this->transport->dispatchWithLongTimeout(
      'order/review',
      $transportData
    );

    $this->_redirect('recapture/review/success');
  }

  private function _translateOrderHash($hash = null){
    if (empty($hash)) {
      return false;
    }

    $result = $this->transport->dispatchWithLongTimeout('order/retrieve', array(
      'hash' => $hash
    ));

    $body = json_decode($result->getBody());

    if ($body->status == 'success'){
      return $body->data->order_id;
    }

    return false;
  }
}