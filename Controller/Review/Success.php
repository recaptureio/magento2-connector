<?php

namespace Recapture\Connector\Controller\Review;

use Recapture\Connector\Model\Landing;

class Success extends \Magento\Framework\App\Action\Action {

  protected $helper;
  protected $transport;
  protected $logger;
  protected $messageManager;
  protected $urlInterface;
  protected $invalidator;

  public function __construct(
    \Magento\Framework\App\Action\Context $context,
    \Recapture\Connector\Helper\Data $helper,
    \Recapture\Connector\Helper\Invalidator $invalidator,
    \Recapture\Connector\Helper\Transport $transport,
    \Psr\Log\LoggerInterface $logger
  ) {
    $this->helper         = $helper;
    $this->invalidator    = $invalidator;
    $this->transport      = $transport;
    $this->logger         = $logger;
    $this->urlInterface   = $context->getUrl();

    parent::__construct($context);
  }

  public function execute(){
    if (!$this->helper->isEnabled() || !$this->helper->getApiKey()){
      return $this->_redirect('/');
    }

    $this->_view->loadLayout();
    $this->_view->renderLayout();
  }
}
