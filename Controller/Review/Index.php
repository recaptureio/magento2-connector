<?php

namespace Recapture\Connector\Controller\Review;

use Recapture\Connector\Model\Landing;

class Index extends \Magento\Framework\App\Action\Action {

  protected $helper;
  protected $transport;
  protected $logger;
  protected $messageManager;
  protected $urlInterface;
  protected $invalidator;

  public function __construct(
    \Magento\Framework\App\Action\Context $context,
    \Recapture\Connector\Helper\Data $helper,
    \Recapture\Connector\Helper\Invalidator $invalidator,
    \Recapture\Connector\Helper\Transport $transport,
    \Psr\Log\LoggerInterface $logger
  ) {
    $this->helper         = $helper;
    $this->invalidator    = $invalidator;
    $this->transport      = $transport;
    $this->logger         = $logger;
    $this->urlInterface   = $context->getUrl();

    parent::__construct($context);
  }

  public function execute(){
    if (!$this->helper->isEnabled() || !$this->helper->getApiKey()){
      return $this->_redirect('/');
    }

    try {
      $hash = $this->getRequest()->getParam('hash');
      $orderId = $this->_translateOrderHash($hash);
    } catch (\Exception $e){
    }

    if (!$orderId) {
      $this->messageManager->addError('There was an error retrieving your order.');
      return $this->_redirect('/');
    }

    $this->_view->loadLayout();
    $block = $this->_view->getLayout()->getBlock('recapture.review');
    $block->setOrderId($orderId);
    $block->setActionUrl($block->getUrl('recapture/review/submit', array('hash' => $hash)));
    $this->_view->renderLayout();
  }

  private function _translateOrderHash($hash = null){
    if (empty($hash)) {
      return false;
    }

    $result = $this->transport->dispatchWithLongTimeout('order/retrieve', array(
      'hash' => $hash
    ));

    $body = json_decode($result->getBody());

    if ($body->status == 'success'){
      return $body->data->order_id;
    }

    return false;
  }
}
