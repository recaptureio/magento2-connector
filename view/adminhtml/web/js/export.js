const log = console.log;

function sendRequest($, ajaxUrl, scope, params) {
  return new Promise((resolve, reject) => {
    ajaxRequest = $.ajax({
      showLoader: true,
      url: `${ajaxUrl}&${jQuery.param(
        Object.assign({}, scope, params)
      )}`,
      timeout: 120000,
      data: {
        form_key: window.FORM_KEY
      },
      type: 'POST',
      dataType: 'json',
      beforeSend: function () {
      }
    });

    // Show successfully for submit message
    ajaxRequest.done(function (response) {
      resolve(response);
    });

    // On failure of request this function will be called
    ajaxRequest.fail(function (error) {
      reject(error);
    });
  });
}

function setStatus($, status) {
  $('#export_status').show();
  $('#export_status').text(status + '...');
}

function sequence(tasks, fn) {
  return tasks.reduce(
    (promise, task) => promise.then(() => fn(task)),
    Promise.resolve()
  );
}

function init($, ajaxUrl, scope) {
  $('#export_button').on('click', function() {
    $('#export_button').hide();
    $('#export_failed').hide();
    $('#export_success').hide();

    Promise.resolve()
      .then(() => setStatus($, 'Exporting abandoned carts'))
      .then(() => sendRequest($, ajaxUrl, scope, { action: 'abandoned_carts' }))
      .then(() => setStatus($, 'Getting order count'))
      .then(() => sendRequest($, ajaxUrl, scope, { action: 'get_order_count' }))
      .then((orderCount) => {
        const pageSize = 50;
        const pageCount = Math.ceil(orderCount / pageSize);

        // process all pages
        const pages = [];
        for (let page = 1; page <= pageCount; page += 1) {
          pages.push(page);
        }

        // export the pages
        return sequence(pages, (page) => {
          setStatus($, `Exporting orders (${page} / ${pageCount})`)
          return sendRequest($, ajaxUrl, scope, {
            action: 'export_orders_page',
            page: page,
            pageSize: pageSize
          });
        });
      })
      .then(() => {
        $('#export_success').show();
        $('#export_status').hide();
      })
      .catch((error) => {
        log('Failed', error);
        $('#export_failed').show();
        $('#export_button').show();
        $('#export_status').hide();
      });
  });
}
