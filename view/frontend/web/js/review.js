(function($, root) {
  $(document).ready(function() {
    $('.product-review .ratings .stars')
      .on('mouseover', '.star', function() {
        el = $(this);

        el.siblings().removeClass('hover');

        el.prevAll().addClass('hover');

        el.nextAll().each(function(e) {
          subEl = $(e);
          if (subEl.hasClass('active')) subEl.addClass('active-hold');
        });

        el.addClass('hover').removeClass('active-hold');
      })
      .on('mouseout', '.star', function() {
        el = $(this);
        el.siblings().removeClass('hover').removeClass('active-hold');

        el.removeClass('hover');
      })
      .on('click', '.star', function() {
        el = $(this);

        el
          .siblings()
          .removeClass('hover')
          .removeClass('active')
          .removeClass('active-hold');
        el.removeClass('hover').removeClass('active-hold').addClass('active');
        el.prevAll().addClass('active');

        var ratingHook = el.attr('data-id');
        $('#' + ratingHook).attr('value', el.attr('data-val'));
      });

    $('#review-form fieldset a.opt-out').click(function() {
      var el = $(this);
      var fieldset = el.parents('fieldset.review-item');
      var reviewForm = fieldset.find('.product-review-form');
      var indicator = fieldset.find('input.skip[type="hidden"]');

      if (!reviewForm.hasClass('invis')) {
        indicator.attr('value', 1);
        el.html(el.data('show'));
        reviewForm
          .hide()
          .addClass('invis')
          .find('input')
          .attr('disabled', true);
      } else {
        indicator.attr('value', 0);
        el.html(el.data('hide'));
        reviewForm
          .show()
          .removeClass('invis')
          .find('input')
          .attr('disabled', false);
      }
    });

    $('#review-form').submit(validateReviewForm);
  });

  window.validateReviewForm = function() {
    valid = true;

    $('#review-form .validation-failed').removeClass('validation-failed');

    $('#review-form fieldset').each(function() {
      el = $(this);

      var skip = el.find('.skip').first();

      if (skip.val() != 1) {
        var summary = el.find('.summary-field');
        var content = el.find('.review-field');

        if (summary.val() == '') {
          valid = false;
          summary.addClass('validation-failed');
        }

        if (content.val() == '') {
          valid = false;
          content.addClass('validation-failed');
        }

        el.find('.rating_value').each(function() {
          var el = $(this);

          if (el.val() == '' || el.val() == 0) {
            valid = false;
            el.parents('.rating').addClass('validation-failed');
          }
        });
      }
    });

    if (valid === false) {
      $('html, body').animate(
        {
          scrollTop: $('.validation-failed').first().offset().top - 30
        },
        100
      );
    }

    return valid;
  };
})(jQuery.noConflict(), window);
