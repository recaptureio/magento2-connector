<?php

namespace Recapture\Connector\Helper;

class Quote extends \Magento\Framework\App\Helper\AbstractHelper {

    protected $helper;
    protected $transport;
    protected $logger;
    protected $storeManager;
    protected $quoteUpdateHistory;
    protected $productResource;
    protected $mediaConfig;
    protected $optionsHelper;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Recapture\Connector\Helper\Data $helper,
        \Recapture\Connector\Helper\Transport $transport,
        \Magento\Catalog\Model\ResourceModel\Product $productResource,
        \Magento\Catalog\Model\Product\Media\Config $mediaConfig,
        \Magento\Catalog\Helper\Product\Configuration $optionsHelper,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->helper = $helper;
        $this->transport = $transport;
        $this->productResource = $productResource;
        $this->mediaConfig = $mediaConfig;
        $this->optionsHelper = $optionsHelper;
        $this->logger = $logger;
        $this->storeManager = $storeManager;
        $this->quoteUpdateHistory = [];

        parent::__construct($context);
    }

    public function getCartFromQuote($quote, $store) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $priceHelper = $objectManager->create('Magento\Framework\Pricing\Helper\Data');
        $mediaConfig = $this->mediaConfig;
        
        $totalWithTax = $this->helper->getStoreConfig('recapture/abandoned_carts/include_tax_with_products');

        $cart = array(
            'first_name' => $this->helper->getCustomerFirstname($quote),
            'last_name' => $this->helper->getCustomerLastname($quote),
            'email' => $this->helper->getCustomerEmail($quote),
            'external_id' => $quote->getId(),
            'grand_total' => $priceHelper->currency($quote->getBaseGrandTotal(), false, false),
            'grand_total_display' => $priceHelper->currency($quote->getBaseGrandTotal(), true, false),
            'phone' => $this->helper->getCustomerPhone($quote),
            'products' => array(),
            'totals' => array()
        );

        $cartItems = $quote->getAllVisibleItems();

        foreach ($cartItems as $item) {
            $productModel = $item->getProduct();

            // get the product categories
            $categories = $productModel
                ->getCategoryCollection()
                ->addAttributeToSelect('name');

            $categoryNames = [];
            $categoryIds = [];

            foreach ($categories as $cat) {
                $parents = $cat->getCollection()
                    ->addIdFilter($cat->getParentIds())
                    ->addAttributeToSelect('name')
                    ->addUrlRewriteToResult()
                    ->setOrder('level');

                foreach ($parents as $parentCat) {
                    $categoryIds[$parentCat->getId()] = $parentCat->getId();
                    $categoryNames[$parentCat->getName()] = $parentCat->getName();
                }

                $categoryIds[$cat->getId()] = $cat->getId();
                $categoryNames[$cat->getName()] = $cat->getName();
            }

            $productImage = false;

            $image = $this->productResource->getAttributeRawValue($productModel->getId(), 'thumbnail', $store);

            if ($image && $image != 'no_selection') {
                $productImage = $mediaConfig->getMediaUrl($image);
            }

            //check configurable first
            if ($item->getProductType() == 'configurable') {
                if ($this->helper->getStoreConfig('checkout/cart/configurable_product_image') == 'itself') {
                    $child = $productModel->getIdBySku($item->getSku());
                    $image = $this->productResource->getAttributeRawValue($child, 'thumbnail', $store);

                    if ($image && $image != 'no_selection') {
                        $productImage = $mediaConfig->getMediaUrl($image);
                    }
                }
            }

            //then check grouped
            if ($this->helper->getStoreConfig('checkout/cart/grouped_product_image') == 'parent') {
                $options = $productModel->getTypeInstance(true)->getOrderOptions($productModel);

                if (isset($options['super_product_config']) && $options['super_product_config']['product_type'] == 'grouped'){
                    $parent = $options['super_product_config']['product_id'];
                    $image = $this->productResource->getAttributeRawValue($parent, 'thumbnail', $store);

                    if ($image && $image != 'no_selection') {
                        $productImage = $mediaConfig->getMediaUrl($image);
                    }
                }
            }

            //if after all that, we still don't have a product image, we get the placeholder image
            if (!$productImage) {
                $productImage = $mediaConfig->getMediaUrl(
                    'placeholder/'.$this->helper->getStoreConfig('catalog/placeholder/image_placeholder')
                );
            }

            $visibleOptions = $this->optionsHelper->getOptions($item);

            $price = $totalWithTax
                ? $item->getBasePriceInclTax()
                : $item->getBasePrice();

            $product = array(
                'name' => $item->getName(),
                'sku' => $item->getSku(),
                'price' => $priceHelper->currency($price, false, false),
                'price_display' => $priceHelper->currency($price, true, false),
                'qty' => $item->getQty(),
                'image' => $productImage,
                'options' => $visibleOptions,
                'category_names'=> array_values($categoryNames),
                'category_ids'=> array_values($categoryIds),
                'product_id' => $item->getProductId()
            );
            $cart['products'][] = $product;
        }

        $totals = $quote->getTotals();

        foreach ($totals as $total) {
            $value = $total->getValue();

            //we pass grand total on the top level
            if ($total->getCode() == 'grand_total' ||
                is_array($value) ||
                is_object($value)) {
                continue;
            }

            $total = array(
                'name' => (string)$total->getTitle(),
                'amount' => (string)$value
            );

            $cart['totals'][] = $total;
        }

        return $cart;
    }

    public function getCartFromOrder($order, $store) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $priceHelper = $objectManager->create('Magento\Framework\Pricing\Helper\Data');
        $mediaConfig = $this->mediaConfig;

        // ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        // error_reporting(E_ALL);
        
        $totalWithTax = $this->helper->getStoreConfig('recapture/abandoned_carts/include_tax_with_products');

        $cart = [
            'first_name' => $this->helper->getOrderFirstname($order),
            'last_name' => $this->helper->getOrderLastname($order),
            'email' => $this->helper->getOrderEmail($order),
            'phone' => $this->helper->getOrderPhone($order),
            'external_id' => $order->getQuoteId(),
            'grand_total' => $priceHelper->currency($order->getBaseGrandTotal(), false, false),
            'grand_total_display' => $priceHelper->currency($order->getBaseGrandTotal(), true, false),
            'products' => [],
            'totals' => []
        ];

        $items = $order->getAllVisibleItems();

        foreach ($items as $item) {
            $productModel = $item->getProduct();

            // get the product categories
            $categories = $productModel
                ->getCategoryCollection()
                ->addAttributeToSelect('name');

            $categoryNames = [];
            $categoryIds = [];

            foreach ($categories as $cat) {
                $parents = $cat->getCollection()
                    ->addIdFilter($cat->getParentIds())
                    ->addAttributeToSelect('name')
                    ->addUrlRewriteToResult()
                    ->setOrder('level');

                foreach ($parents as $parentCat) {
                    $categoryIds[$parentCat->getId()] = $parentCat->getId();
                    $categoryNames[$parentCat->getName()] = $parentCat->getName();
                }

                $categoryIds[$cat->getId()] = $cat->getId();
                $categoryNames[$cat->getName()] = $cat->getName();
            }

            $productImage = false;

            $image = $this->productResource->getAttributeRawValue($productModel->getId(), 'thumbnail', $store);

            if ($image && $image != 'no_selection') {
                $productImage = $mediaConfig->getMediaUrl($image);
            }

            //check configurable first
            if ($item->getProductType() == 'configurable') {
                if ($this->helper->getStoreConfig('checkout/cart/configurable_product_image') == 'itself') {
                    $child = $productModel->getIdBySku($item->getSku());
                    $image = $this->productResource->getAttributeRawValue($child, 'thumbnail', $store);

                    if ($image && $image != 'no_selection') {
                        $productImage = $mediaConfig->getMediaUrl($image);
                    }
                }
            }

            //then check grouped
            if ($this->helper->getStoreConfig('checkout/cart/grouped_product_image') == 'parent') {
                $options = $productModel->getTypeInstance(true)->getOrderOptions($productModel);

                if (isset($options['super_product_config']) && $options['super_product_config']['product_type'] == 'grouped'){
                    $parent = $options['super_product_config']['product_id'];
                    $image = $this->productResource->getAttributeRawValue($parent, 'thumbnail', $store);

                    if ($image && $image != 'no_selection') {
                        $productImage = $mediaConfig->getMediaUrl($image);
                    }
                }
            }

            //if after all that, we still don't have a product image, we get the placeholder image
            if (!$productImage) {
                $productImage = $mediaConfig->getMediaUrl(
                    'placeholder/'.$this->helper->getStoreConfig('catalog/placeholder/image_placeholder')
                );
            }

            $price = $totalWithTax
                ? $item->getBasePriceInclTax()
                : $item->getBasePrice();

            $product = [
                'name' => $item->getName(),
                'sku' => $item->getSku(),
                'price' => $priceHelper->currency($price, false, false),
                'price_display' => $priceHelper->currency($price, true, false),
                'qty' => $item->getQtyOrdered(),
                'image' => $productImage,
                'category_names'=> array_values($categoryNames),
                'category_ids'=> array_values($categoryIds),
                'product_id' => $item->getProductId()
            ];

            $cart['products'][] = $product;
        }

        $cart['totals'] = $this->helper->getOrderTotals($order);

        return $cart;
    }

    public function updateQuote($quote, $from = '') {
        $start = microtime(true);
        $quote_id = $quote->getId();

        if (!$this->helper->isEnabledForCartUpdates()) {
            return $this;
        }

        if (!$quote->getId()) {
            return;
        }

        if ($this->helper->isBoltActive() && !$quote->getIsActive()) {
            // Ignoring cart because Bolt is active but cart is inactive
            return;
        }

        if (
            isset($this->quoteUpdateHistory[$quote_id])
            && $this->quoteUpdateHistory[$quote_id] == true
        ) {
            /*
            $this->logger->info(
                "Recapture ($from): We've already sent this quote, ignoring (Quote:$quote_id)"
            );
            */
            return;
        }

        // Save the cart update
        $this->quoteUpdateHistory[$quote_id] = true;

        // Get the cart object from the quote
        $cart = $this->getCartFromQuote($quote, $this->storeManager->getStore());

        /*
        $time = (microtime(true) - $start) * 1000;
        $this->logger->info("Recapture ($from): Loaded cart data from db {$time}ms (Quote:$quote_id)");
        */

        // See if we've already sent this quote update in this request
        // if so, ignore it to avoid duplicates. quoteUpdateHistory will be
        // reset at the next request so we don't need to worry about checking
        // update times

        // Send the cart to Recapture
        $this->transport->dispatch('cart', $cart);

        /*
        $time = (microtime(true) - $start) * 1000;
        $this->logger->info("Recapture ($from): Sent cart to Recapture in {$time}ms (Quote:$quote_id)");
        */
        return $this;
    }
}
