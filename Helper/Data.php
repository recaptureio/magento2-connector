<?php

namespace Recapture\Connector\Helper;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Config\Model\Config;
use Magento\Store\Model\Store;
use Magento\Store\Model\Website;
use Magento\Quote\Model\QuoteFactory;
use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Escaper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    protected $scopeConfigWriter;
    protected $scopeConfig;
    protected $checkoutSession;
    protected $storeModel;
    protected $websiteModel;
    protected $moduleList;
    protected $quoteFactory;
    protected $visitorSession;
    protected $customerMetadataService;
    protected $escaper;
    protected $priceHelper;
    protected $customerSession;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Config\Storage\Writer $scopeConfigWriter,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Session\SessionManagerInterface $session,
        CheckoutSession $checkoutSession,
        CustomerSession $customerSession,
        Store $storeModel,
        Website $websiteModel,
        QuoteFactory $quoteFactory,
        CustomerMetadataInterface $customerMetadataService,
        Escaper $escaper = null,
        \Magento\Framework\Pricing\Helper\Data $priceHelper
    ) {
        $this->scopeConfig       = $context->getScopeConfig();
        $this->scopeConfigWriter = $scopeConfigWriter;
        $this->customerSession   = $customerSession;
        $this->checkoutSession   = $checkoutSession;
        $this->visitorSession    = $session;
        $this->storeModel        = $storeModel;
        $this->websiteModel      = $websiteModel;
        $this->quoteFactory      = $quoteFactory;
        $this->moduleList        = $moduleList;
        $this->customerMetadataService = $customerMetadataService;
        $this->escaper = $escaper ?? ObjectManager::getInstance()->get(Escaper::class);
        $this->priceHelper = $priceHelper;

        parent::__construct($context);
    }

    public function getVersion(){

        return $this->moduleList->getOne('Recapture_Connector')['setup_version'];

    }

    public function getConfig($configKey, $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeCode = null){

        return $this->scopeConfig->getValue($configKey, $scope, $scopeCode);

    }

    public function getStoreConfig($configKey){
        return $this->storeModel->getConfig($configKey);
    }

    public function saveConfig($path, $value, $scope, $scopeId){

        return $this->scopeConfigWriter->save($path, $value, $scope, $scopeId);

    }

    public function deleteConfig($path, $scope, $scopeId){
        return $this->scopeConfigWriter->delete($path, $scope, $scopeId);
    }

    public function isEnabledForCartUpdates() {
        $ignoreBackendUsers = $this->getStoreConfig('recapture/configuration/ignore_backend_users');

        if ($ignoreBackendUsers && isset($_COOKIE['recapture_backend_user'])) {
            return false;
        }

        return $this->getStoreConfig('recapture/configuration/enabled');
    }

    public function canAuthenticateDefaultView() {
        return $this->getStoreConfig('recapture/advanced/allow_auth_default');
    }

    public function isEnabled(){
        return $this->getStoreConfig('recapture/configuration/enabled');
    }

    public function shouldCaptureSubscriber(){

        return $this->getStoreConfig('recapture/abandoned_carts/capture_subscriber');

    }

    public function getHomeUrl($path){

        $baseUrl = $this->getStoreConfig('recapture/configuration/dev_base_url');
        if (!$baseUrl) $baseUrl = 'https://www.recapture.io/';

        return $baseUrl . $path;

    }

    public function getAnalyticsUrl(){

        $queueUrl = $this->getStoreConfig('recapture/configuration/dev_analytics_url');
        if (!$queueUrl) $queueUrl = '//cdn.recapture.io/sdk/v1/magento2-recapture.min.js';

        return $queueUrl;

    }

    public function canTrackEmail(){

        return $this->getStoreConfig('recapture/abandoned_carts/track_email');

    }

    public function getReturnLanding(){

        return $this->getStoreConfig('recapture/abandoned_carts/return_landing');

    }

    public function getApiKey(){

        return $this->getStoreConfig('recapture/configuration/api_key');

    }

    public function getExtensionAuthToken() {
        return $this->getStoreConfig('recapture/configuration/extension_auth_token');
    }

    public function setExtensionAuthToken($token) {
        $scope = $this->getCurrentScope();
        $scopeId = $this->getCurrentScopeId();

        return $this->saveConfig(
            'recapture/configuration/extension_auth_token',
            $token,
            $scope,
            $scopeId
        );
    }

    public function deleteExtensionAuthToken() {
        $scope = $this->getCurrentScope();
        $scopeId = $this->getCurrentScopeId();

        return $this->deleteConfig(
            'recapture/configuration/extension_auth_token',
            $scope,
            $scopeId
        );
    }

    public function isBoltActive() {
        return (
            $this->_moduleManager->isEnabled('Bolt_Boltpay') &&
            $this->getStoreConfig('payment/boltpay/active') == 1
        );
    }

    public function getActiveWebsite(){

        $website = $this->_request->getParam('website');
        $website = !empty($website) ? $website : null;

        return $website;

    }

    public function getActiveStore(){

        $store = $this->_request->getParam('store');
        $store = !empty($store) ? $store : null;

        return $store;

    }


    public function getScopeStoreId(){

        $website = $this->getActiveWebsite();
        $store   = $this->getActiveStore();

        if (!$website && !$store) return '0';

        if ($store) return $this->storeModel->load($store)->getId();
        if ($website) return $this->websiteModel->load($website)->getDefaultGroup()->getDefaultStoreId();



    }

    public function getCurrentScope(){

        $website = $this->getActiveWebsite();
        $store   = $this->getActiveStore();

        if (!$website && !$store) return 'default';

        if ($store) return 'stores';
        if ($website) return 'websites';

    }

    public function getScopeForUrl(){

        $website = $this->getActiveWebsite();
        $store   = $this->getActiveStore();

        if (!$website && !$store) return array();

        if ($store) return array('website' => $website, 'store' => $store);
        if ($website) return array('website' => $website);

    }

    public function getCurrentScopeId(){

        $website = $this->getActiveWebsite();
        $store   = $this->getActiveStore();

        if (!$website && !$store) return 0;

        if ($store) return $this->storeModel->load($store)->getId();
        if ($website) return $this->websiteModel->load($website)->getId();

    }

    public function associateCartToMe($cartId = null, $couponCode = null) {

        if (empty($cartId)) {
            return false;
        }

        $quote = $this->quoteFactory->create()->load($cartId);

        //if quote has a customer id, log them in
        $this->checkoutSession->replaceQuote($quote);

        $quote = $this->checkoutSession->getQuote();

        //if this cart somehow was already converted, we're not going to be able to load it. as such, we can't associate it.
        if ($quote->getId() != $cartId) {
            return false;
        }

        // apply the coupon code
        try {
            if (!empty($couponCode)) {
                $quote
                    ->setCouponCode($couponCode)
                    ->setTotalsCollectedFlag(false)
                    ->collectTotals()
                    ->save();
            }
        } catch (\Exception $e) {
        }

        return true;

    }

    public function getCustomerFirstname(\Magento\Quote\Model\Quote $quote){

        //we first check the quote model itself
        $customerFirstname = $quote->getCustomerFirstname();
        if (!empty($customerFirstname)) return $customerFirstname;

        //if not on the quote model, we check the billing address
        $billingAddress = $quote->getBillingAddress();
        if ($billingAddress){

            $customerFirstname = $billingAddress->getFirstname();
            if (!empty($customerFirstname)) return $customerFirstname;

        }

        //if not in the billing address, last resort we check the shipping address
        $shippingAddress = $quote->getShippingAddress();
        if ($shippingAddress){

            $customerFirstname = $shippingAddress->getFirstname();
            if (!empty($customerFirstname)) return $customerFirstname;

        }

        return null;

    }

    public function getCustomerLastname(\Magento\Quote\Model\Quote $quote){

        //we first check the quote model itself
        $customerLastname = $quote->getCustomerLastname();
        if (!empty($customerLastname)) return $customerLastname;

        //if not on the quote model, we check the billing address
        $billingAddress = $quote->getBillingAddress();
        if ($billingAddress){

            $customerLastname = $billingAddress->getLastname();
            if (!empty($customerLastname)) return $customerLastname;

        }

        //if not in the billing address, last resort we check the shipping address
        $shippingAddress = $quote->getShippingAddress();
        if ($shippingAddress){

            $customerLastname = $shippingAddress->getLastname();
            if (!empty($customerLastname)) return $customerLastname;

        }

        return null;

    }

    public function getCustomerEmail(\Magento\Quote\Model\Quote $quote) {

        //we first check the quote model itself
        $customerEmail = $quote->getCustomerEmail();

        if (!empty($customerEmail)) {
            return $customerEmail;
        }

        //if not on the quote model, we check the billing address
        $billingAddress = $quote->getBillingAddress();

        if ($billingAddress) {
            $customerEmail = $billingAddress->getEmail();

            if (!empty($customerEmail)) {
                return $customerEmail;
            }
        }

        //if not in the billing address, last resort we check the shipping address
        $shippingAddress = $quote->getShippingAddress();

        if ($shippingAddress) {
            $customerEmail = $shippingAddress->getEmail();

            if (!empty($customerEmail)) {
                return $customerEmail;
            }
        }

        return null;
    }

    public function getCustomerPhone(\Magento\Quote\Model\Quote $quote) {
        //if not on the quote model, we check the billing address
        $billingAddress = $quote->getBillingAddress();

        if ($billingAddress) {
            $phone = $billingAddress->getTelephone();

            if (!empty($phone)) {
                return $phone;
            }
        }

        //if not in the billing address, last resort we check the shipping address
        $shippingAddress = $quote->getShippingAddress();

        if ($shippingAddress) {
            $phone = $shippingAddress->getEmail();

            if (!empty($phone)) {
                return $phone;
            }
        }

        return null;
    }


    public function getOrderFirstname($order) {
        //we first check the order model itself
        $customerFirstname = $order->getCustomerFirstname();

        if (!empty($customerFirstname)) {
            return $customerFirstname;
        }

        //if not on the order model, we check the billing address
        $billingAddress = $order->getBillingAddress();
        if ($billingAddress) {
            $customerFirstname = $billingAddress->getFirstname();

            if (!empty($customerFirstname)) {
                return $customerFirstname;
            }
        }

        //if not in the billing address, last resort we check the shipping address
        $shippingAddress = $order->getShippingAddress();
        if ($shippingAddress) {
            $customerFirstname = $shippingAddress->getFirstname();

            if (!empty($customerFirstname)){
                return $customerFirstname;
            }
        }

        return null;
    }

    public function getOrderLastname($order){
        //we first check the order model itself
        $customerLastname = $order->getCustomerLastname();
        if (!empty($customerLastname)) {
            return $customerLastname;
        }

        // if not on the order model, we check the billing address
        $billingAddress = $order->getBillingAddress();
        if ($billingAddress) {
            $customerLastname = $billingAddress->getLastname();
            if (!empty($customerLastname)) {
                return $customerLastname;
            }
        }

        // if not in the billing address, last resort we check the shipping address
        $shippingAddress = $order->getShippingAddress();
        if ($shippingAddress) {
            $customerLastname = $shippingAddress->getLastname();
            if (!empty($customerLastname)) {
                return $customerLastname;
            }
        }

        return null;
    }

    public function getOrderEmail($order) {
        // we first check the order model itself
        $customerEmail = $order->getCustomerEmail();
        if (!empty($customerEmail)) {
            return $customerEmail;
        }

        // if not on the order model, we check the billing address
        $billingAddress = $order->getBillingAddress();
        if ($billingAddress) {
            $customerEmail = $billingAddress->getEmail();
            if (!empty($customerEmail)) {
                return $customerEmail;
            }
        }

        // if not in the billing address, last resort we check the shipping address
        $shippingAddress = $order->getShippingAddress();
        if ($shippingAddress) {
            $customerEmail = $shippingAddress->getEmail();
            if (!empty($customerEmail)) {
                return $customerEmail;
            }
        }

        return null;
    }

    public function getOrderPhone($order) {
        //if not on the quote model, we check the billing address
        $billingAddress = $order->getBillingAddress();

        if ($billingAddress) {
            $phone = $billingAddress->getTelephone();

            if (!empty($phone)) {
                return $phone;
            }
        }

        //if not in the billing address, last resort we check the shipping address
        $shippingAddress = $order->getShippingAddress();

        if ($shippingAddress) {
            $phone = $shippingAddress->getEmail();

            if (!empty($phone)) {
                return $phone;
            }
        }

        return null;
    }

    public function getCustomerHash(){

        return isset($_COOKIE['ra_customer_id']) ? $_COOKIE['ra_customer_id'] : null;

    }

    // Taken from Magento source
    public function getCustomerName($customerData) {
        $name = '';
        $prefixMetadata = $this->customerMetadataService->getAttributeMetadata('prefix');
        if ($prefixMetadata->isVisible() && $customerData->getPrefix()) {
            $name .= __($customerData->getPrefix()) . ' ';
        }

        $name .= $customerData->getFirstname();

        $middleNameMetadata = $this->customerMetadataService->getAttributeMetadata('middlename');
        if ($middleNameMetadata->isVisible() && $customerData->getMiddlename()) {
            $name .= ' ' . $customerData->getMiddlename();
        }

        $name .= ' ' . $customerData->getLastname();

        $suffixMetadata = $this->customerMetadataService->getAttributeMetadata('suffix');
        if ($suffixMetadata->isVisible() && $customerData->getSuffix()) {
            $name .= ' ' . __($customerData->getSuffix());
        }

        return $this->escaper->escapeHtml($name);
    }

    public function dumpVariableToString() {
        $argc = func_num_args();
        $argv = func_get_args();

        if ($argc > 0) {
            ob_start();
            call_user_func_array('var_dump', $argv);
            $result = ob_get_contents();
            ob_end_clean();
            return $result;
        }

        return '';
    }

    public function getAddress($address) {
        $street = $address->getStreet();

        return [
            'address1' => $street[0] ?? '',
            'address2' => $street[1] ?? '',
            'city' => $address->getCity(),
            'country' => $address->getCountryId(),
            'region' => $address->getRegion(),
            'zip' => $address->getPostcode(),
            'name' => $this->getCustomerName($address),
            'company' => $address->getCompany(),
            'email' => $address->getEmail(),
            'phone' => $address->getTelephone()
        ];
    }

    // Taken from Magento:
    // app/code/Magento/Sales/Block/Order/Totals.php

    public function getOrderTotals($order) {
        $totals = [];

        $totals['subtotal'] = [
            'code' => 'subtotal',
            'amount' => $order->getSubtotal(),
            'amount_display' => $this->priceHelper->currency($order->getSubtotal(), true, false),
            'name' => 'Subtotal'
        ];

        if ((double)$order->getDiscountAmount() != 0) {
            if ($order->getDiscountDescription()) {
                $discountLabel = 'Discount ('.$order->getDiscountDescription().')';
            } else {
                $discountLabel = 'Discount';
            }

            $totals['discount'] =[
                'code' => 'discount',
                'amount' => $order->getDiscountAmount(),
                'amount_display' => $this->priceHelper->currency($order->getDiscountAmount(), true, false),
                'name' => $discountLabel,
            ];
        }

        if (!$order->getIsVirtual() && ($order->getShippingAmount() !== null || $order->getShippingDescription())) {
            $shippingLabel = 'Shipping & Handling';

            if (!isset($totals['discount'])) {
                if ($order->getCouponCode()) {
                    $shippingLabel .= " ({$order->getCouponCode()})";
                } elseif ($order->getDiscountDescription()) {
                    $shippingLabel .= " ({$order->getDiscountDescription()})";
                }
            }

            $totals['shipping'] = [
                'code' => 'shipping',
                'amount' => $order->getShippingAmount(),
                'amount_display' => $this->priceHelper->currency($order->getShippingAmount(), true, false),
                'name' => $shippingLabel,
            ];
        }

        $totals['tax'] = [
            'code' => 'tax',
            'amount' => $order->getTaxAmount(),
            'amount_display' => $this->priceHelper->currency($order->getTaxAmount(), true, false),
            'name' => 'Tax'
        ];

        $totals['grand_total'] = [
            'code' => 'grand_total',
            'amount' => $order->getGrandTotal(),
            'amount_display' => $this->priceHelper->currency($order->getGrandTotal(), true, false),
            'name' => 'Grand Total',
        ];

        return array_values($totals);
    }
}
