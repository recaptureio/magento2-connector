<?php

namespace Recapture\Connector\Helper;

class Transport extends \Magento\Framework\App\Helper\AbstractHelper {

    protected $helper;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Recapture\Connector\Helper\Data $helper
    ) {
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function dispatch($route = '', $data = array(), $timeout = 2) {

        $response = null;

        try {
          if (!$this->helper->isEnabled()) return false;
          if (empty($route)) return false;

          $client = class_exists('\Laminas\Http\Client')
            ? new \Laminas\Http\Client($this->helper->getHomeUrl('beacon/'.$route), ['timeout' => $timeout])
            : new \Zend\Http\Client($this->helper->getHomeUrl('beacon/'.$route), ['timeout' => $timeout]);

          $data['customer'] = $this->helper->getCustomerHash();
          $data['timestamp'] = time();

          $client->setMethod('POST');
          $client->setParameterPost($data);

          $client->setHeaders(array('Api-Key' => $this->helper->getApiKey()));
          $response = $client->send();

        } catch (\Exception $e){
        }

        return $response;

    }

    public function dispatchWithLongTimeout($route = '', $data = array()) {
        return $this->dispatch($route, $data, 60);
    }
}
