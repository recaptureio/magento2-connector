<?php

namespace Recapture\Connector\Helper;

class Api extends \Magento\Framework\App\Helper\AbstractHelper {
    protected $transport;
    protected $helper;
    protected $cache;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Recapture\Connector\Helper\Transport $transport,
        \Recapture\Connector\Helper\Data $helper,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
    ) {
        $this->transport = $transport;
        $this->helper = $helper;
        $this->cache = $cacheTypeList;

        parent::__construct($context);
    }

    public function validateAuthToken($token) {
        // Check if the key matches what we have stored
        if ($token == null || $token == false || strlen($token) == 0) {
            return false;
        }

        // If we don't have anything stored then check against recapture
        $savedToken = $this->helper->getExtensionAuthToken();

        if ($savedToken != null && strlen($savedToken) > 0 && $token === $savedToken) {
            return true;
        }

        // Validate the token with Recapture
        $result = $this->transport->dispatchWithLongTimeout('verify-auth-token', [
            'token' => $token
        ]);

        $is_valid = json_decode($result->getBody());

        if ($is_valid == null || $is_valid->ok == false) {
            return false;
        }

        // Store the value in the cache for next time
        $this->helper->setExtensionAuthToken($token);

        // Clear the cache
        $this->cache->cleanType('config');

        // OK!
        return true;
    }

    public function resetAuthToken() {
        $this->helper->deleteExtensionAuthToken();

        // Clear the cache
        $this->cache->cleanType('config');
    }
}
