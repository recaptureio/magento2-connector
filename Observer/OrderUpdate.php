<?php
namespace Recapture\Connector\Observer;

class OrderUpdate implements \Magento\Framework\Event\ObserverInterface {

    protected $helper;
    protected $transport;
    protected $logger;

    public function __construct(
        \Recapture\Connector\Helper\Data $helper,
        \Recapture\Connector\Helper\Transport $transport,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->helper      = $helper;
        $this->transport   = $transport;
        $this->logger      = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer){
        if (!$this->helper->isEnabled())
          return $this;

        try {
            $order = $observer->getEvent()->getOrder();

            // if ($order->getState() == $order->getOrigData('state')) {
            //   return $this;
            // }

            $shipping_address = $order->getShippingAddress();
            $billing_address = $order->getBillingAddress();

            // get the payment method
            $payment = $order->getPayment();
            $paymentMethod = $payment->getMethodInstance();

            $transportData = array(
              'external_id' => $order->getIncrementId(),
              'state' => $order->getState(),
              'status'=> $order->getStatus(),
              'shipping_address' => $shipping_address
                ? $this->helper->getAddress($shipping_address)
                : null,
              'billing_address' => $billing_address
                  ? $this->helper->getAddress($billing_address)
                  : null,
              'payment_method' => [
                  'code' => $paymentMethod->getCode(),
                  'name' => $paymentMethod->getTitle()
              ],
              'shipping_method' => [
                  'code' => $order->getShippingMethod(),
                  'name' => $order->getShippingDescription()
              ],
              'totals' => $this->helper->getOrderTotals($order)
            );

            $this->transport->dispatch('order/update', $transportData);
        } catch (\Exception $e){
            $this->logger->critical($e);
        }
        return $this;
    }
}
