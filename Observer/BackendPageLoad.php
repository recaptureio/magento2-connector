<?php
namespace Recapture\Connector\Observer;

class BackendPageLoad implements \Magento\Framework\Event\ObserverInterface {
    protected $session;
    protected $hasSetCookie = false;

    public function __construct(\Magento\Backend\Model\Auth\Session $authSession) {
        $this->session = $authSession;
    }

    public function execute(\Magento\Framework\Event\Observer $observer){
        if ($this->session->isLoggedIn() && !$this->hasSetCookie && !headers_sent()) {
            setcookie('recapture_backend_user', '1', time() + (60 * 60), '/');
            $this->hasSetCookie = true;
        }
        return $this;
    }
}
