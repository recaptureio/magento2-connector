<?php
namespace Recapture\Connector\Observer;

class QuoteConversion implements \Magento\Framework\Event\ObserverInterface {

    protected $helper;
    protected $transport;
    protected $logger;

    public function __construct(
        \Recapture\Connector\Helper\Data $helper,
        \Recapture\Connector\Helper\Transport $transport,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->helper      = $helper;
        $this->transport   = $transport;
        $this->logger      = $logger;
    }

    function getAddress($address) {
        $street = $address->getStreet();

        return [
            'address1' => $street[0] ?? '',
            'address2' => $street[1] ?? '',
            'city' => $address->getCity(),
            'country' => $address->getCountryId(),
            'region' => $address->getRegion(),
            'zip' => $address->getPostcode(),
            'name' => $this->helper->getCustomerName($address),
            'company' => $address->getCompany(),
            'email' => $address->getEmail(),
            'phone' => $address->getTelephone()
        ];
    }

    public function execute(\Magento\Framework\Event\Observer $observer){

        if (!$this->helper->isEnabledForCartUpdates()){
            return $this;
        }

        try {
            $order = $observer->getEvent()->getOrder();

            $shipping_address = $order->getShippingAddress();
            $billing_address = $order->getBillingAddress();

            // get the payment method
            $payment = $order->getPayment();
            $paymentMethod = $payment->getMethodInstance();

            $transportData = array(
                'external_id'  => $order->getQuoteId(),
                'order_id' => $order->getIncrementId(),
                'shipping_address' => $shipping_address
                    ? $this->getAddress($shipping_address)
                    : null,
                'billing_address' => $billing_address
                    ? $this->getAddress($billing_address)
                    : null,
                'payment_method' => [
                    'code' => $paymentMethod->getCode(),
                    'name' => $paymentMethod->getTitle()
                ],
                'shipping_method' => [
                    'code' => $order->getShippingMethod(),
                    'name' => $order->getShippingDescription()
                ],
                'totals' => $this->helper->getOrderTotals($order)
            );

            $this->transport->dispatch('conversion', $transportData);
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }

        return $this;

    }

}
