<?php
namespace Recapture\Connector\Observer;

class CheckoutCartSaveAfter implements \Magento\Framework\Event\ObserverInterface {
    protected $quoteHelper;
    protected $logger;
    
    public function __construct(
        \Recapture\Connector\Helper\Quote $quoteHelper,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->quoteHelper = $quoteHelper;
        $this->logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $event = $observer->getEvent();
        if ($event->getCart()){
            $quote = $event->getCart()->getQuote();
        } else {
            $quote = $event->getQuote();
        }

        try {
            $this->quoteHelper->updateQuote($quote, 'CheckoutCartSaveAfter');
        } catch (\Exception $e){
            $this->logger->critical($e);
        }

        return $this;
    }
}
