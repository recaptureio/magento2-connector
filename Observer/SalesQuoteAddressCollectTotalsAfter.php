<?php
namespace Recapture\Connector\Observer;

class SalesQuoteAddressCollectTotalsAfter implements \Magento\Framework\Event\ObserverInterface {
    protected $quoteHelper;
    protected $logger;
    protected $request;
    
    public function __construct(
        \Recapture\Connector\Helper\Quote $quoteHelper,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->quoteHelper = $quoteHelper;
        $this->logger = $logger;
        $this->request = $request;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $event = $observer->getEvent();
        if ($event->getCart()){
            $quote = $event->getCart()->getQuote();
        } else {
            $quote = $event->getQuote();
        }

        $module = $this->request->getModuleName();
        $action = $this->request->getActionName();
        $controller = $this->request->getControllerName();

        if ($module == 'checkout' && in_array($action, ['removeItem', 'add'])) {
            // $this->logger->info("Recapture (SalesQuoteAddressCollectTotalsAfter): Ignoring $module/$action");
            return;
        }

        try {
            $this->quoteHelper->updateQuote($quote, 'SalesQuoteAddressCollectTotalsAfter');
        } catch (\Exception $e){
            $this->logger->critical($e);
        }

        return $this;
    }
}
